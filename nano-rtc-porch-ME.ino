// Date and time functions using a DS3231 RTC connected via I2C and Wire lib
#include "RTClib.h"
RTC_DS3231 rtc;

//pin assignments
byte light = 2;
byte lightswitch = 3;
byte functionswitch = 4; //acts as an automatic/manual mode

//flag for a section of the program in the while loop that should only run once when the program starts
byte run_once_flag = 0;

//variables for loops
unsigned long previousMillis = 0;
const long interval = 45000; //45 seconds (45000) ensures it runs at least once per minute LOWER THIS TO 100 FOR TESTING PURPOSES. used to be 25 seconds in old program
unsigned long previousMillis3 = 0;
const long interval3 = 600000; // 10 minute timer (600000) to turn the light back on automatically after taking the dog out etc. 
unsigned long currentMillis3 = 0;

byte light_state; //keep track of whether the light is on or off regardless of how it was toggled
byte switch_state = 1; //keeps track of the switch state and turns the switch into a toggle 
byte last_switch_state = 1; 
//5-minute timer flag to turn the light back on after a 5-minute timeframe for letting the dog out
byte switch_flag = 0; //switch_flag keeps track of whether or not the light was turned on automatically by the program or manually by the switch
byte timer_flag = 0; //keeps track of the "interval3"-length timer and helps turn the light back on after it was turned off

//note - any minutes randomized need LONGs because the random number generator uses longs
int day_number; //needs to be int because it could be a value larger than "byte" can hold (up to 367, byte is only 256)
byte sunrise_hour;
byte sunset_hour;

//civil twilight times
byte sunrise_minutes[] = {41,41,41,41,41,41,41,41,41,41,40,40,40,39,39,38,38,37,37,36,35,35,34,33,32,32,31,30,29,28,27,26,25,24,23,21,20,19,18,17,15,14,13,11,10,8,7,6,4,3,1,0,58,56,55,53,52,50,48,47,47,45,43,42,40,38,36,35,33,31,29,27,26,24,22,20,18,17,15,13,11,9,7,5,4,2,0,58,56,54,52,50,49,47,45,43,41,39,37,36,34,32,30,28,27,25,23,21,19,18,16,14,12,11,9,7,6,4,2,1,59,58,56,55,53,52,50,49,47,46,44,43,42,41,39,38,37,36,34,33,32,31,30,29,28,27,27,26,25,24,23,23,22,21,21,20,20,19,19,19,18,18,18,18,18,17,17,17,17,17,18,18,18,18,18,19,19,20,20,20,21,21,22,23,23,24,25,26,26,27,28,29,30,31,32,32,33,34,36,37,38,39,40,41,42,43,44,46,47,48,49,50,52,53,54,55,57,58,59,0,2,3,4,5,7,8,9,10,12,13,14,15,17,18,19,20,22,23,24,25,27,28,29,30,32,33,34,35,36,38,39,40,41,43,44,45,46,47,48,50,51,52,53,54,56,57,58,59,0,2,3,4,5,6,8,9,10,11,12,13,15,16,17,18,19,21,22,23,24,26,27,28,29,30,32,33,34,35,37,38,39,40,42,43,44,46,47,48,49,51,52,53,54,56,57,58,59,1,2,3,4,6,7,8,9,10,12,13,14,15,16,17,18,20,21,22,23,24,25,26,27,28,29,29,30,31,32,33,33,34,35,36,36,37,37,38,38,39,39,40,40,40,41,41,41,41,41};
byte sunset_minutes[] = {43,44,45,46,47,48,49,50,51,52,53,54,55,57,58,59,0,1,2,4,5,6,7,9,10,11,13,14,15,16,18,19,20,22,23,24,26,27,28,30,31,32,34,35,36,38,39,40,42,43,44,45,47,48,49,51,52,53,55,56,56,57,58,0,1,2,4,5,6,7,9,10,11,12,14,15,16,18,19,20,21,23,24,25,26,28,29,30,31,33,34,35,37,38,39,40,42,43,44,46,47,48,50,51,52,53,55,56,57,59,0,1,3,4,5,7,8,9,11,12,13,15,16,17,19,20,21,23,24,25,26,28,29,30,32,33,34,35,36,38,39,40,41,42,43,44,46,47,48,49,50,51,51,52,53,54,55,56,56,57,58,58,59,0,0,1,1,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,2,2,1,1,1,0,59,59,58,58,57,56,55,54,53,53,52,51,50,49,47,46,45,44,43,42,40,39,38,36,35,34,32,31,29,28,26,25,23,21,20,18,17,15,13,12,10,8,6,5,3,1,59,57,56,54,52,50,48,47,45,43,41,39,37,35,33,31,30,28,26,24,22,20,18,16,14,12,11,9,7,5,3,1,59,57,56,54,52,50,48,46,45,43,41,39,37,36,34,32,30,29,27,25,24,22,20,19,17,16,14,12,11,9,8,7,5,4,2,1,0,58,57,56,54,53,52,51,50,49,48,47,46,45,44,43,42,41,40,40,39,38,38,37,36,36,35,35,34,34,34,33,33,33,33,33,32,32,32,32,32,33,33,33,33,33,34,34,34,35,35,36,36,37,37,38,39,39,40,41,41,42,42};
//sunset/sunrise times
//byte sunset_minutes[] = {10,11,12,13,14,15,16,18,19,20,21,22,23,25,26,27,28,30,31,32,34,35,36,38,39,40,42,43,44,46,47,49,50,51,53,54,56,57,58,0,1,3,4,5,7,8,10,11,12,14,15,16,18,19,20,22,23,24,26,27,28,30,31,32,34,35,36,37,39,40,41,43,44,45,46,48,49,50,51,53,54,55,56,58,59,0,1,3,4,5,6,7,9,10,11,12,14,15,16,17,18,20,21,22,23,25,26,27,28,30,31,32,33,34,36,37,38,39,41,42,43,44,45,47,48,49,50,51,52,54,55,56,57,58,59,0,1,3,4,5,6,7,8,9,10,11,12,12,13,14,15,16,17,17,18,19,20,20,21,22,22,23,23,24,24,25,25,25,26,26,26,26,26,27,27,27,27,27,27,27,26,26,26,26,26,25,25,24,24,23,23,22,22,21,21,20,19,18,18,17,16,15,14,13,12,11,10,9,8,7,5,4,3,2,0,59,58,56,55,54,52,51,49,48,46,45,43,42,40,38,37,35,33,32,30,28,27,25,23,21,20,18,16,14,13,11,9,7,5,3,2,0,58,56,54,52,50,49,47,45,43,41,39,37,36,34,32,30,28,26,24,23,21,19,17,15,13,12,10,8,6,4,3,1,59,57,56,54,52,51,49,47,46,44,43,41,39,38,36,35,33,32,31,29,28,26,25,24,22,21,20,19,18,17,15,14,13,12,11,10,9,9,8,7,6,5,5,4,4,3,2,2,1,1,1,0,0,0,0,0,59,59,59,59,59,0,0,0,0,0,1,1,1,2,2,3,3,4,5,5,6,7,8,8,9,9};
//byte sunrise_minutes[] = {14,14,14,14,14,14,14,14,13,13,13,12,12,11,11,10,10,9,8,8,7,6,5,4,4,3,2,1,0,59,59,56,55,54,53,52,50,49,48,46,45,44,42,41,39,38,36,35,33,32,30,29,27,25,24,22,20,19,17,15,14,12,10,8,7,5,3,1,0,58,56,54,52,50,49,47,45,43,41,39,38,36,34,32,30,28,27,25,23,21,19,17,16,14,12,10,8,7,5,3,1,0,58,56,55,53,51,49,48,46,45,43,41,40,38,37,35,34,32,31,29,28,26,25,24,22,21,20,18,14,16,15,14,12,11,10,9,8,7,6,5,5,4,3,2,1,1,0,59,59,58,58,57,57,56,56,55,55,55,55,54,54,54,54,54,54,54,54,54,54,55,55,55,55,56,56,56,57,57,58,58,59,59,0,0,1,2,2,3,4,5,6,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,30,31,32,33,34,35,36,38,39,40,41,42,43,44,46,47,48,49,50,51,53,54,55,56,57,58,59,1,2,3,4,5,6,8,9,10,11,12,13,14,16,17,18,19,20,21,23,24,25,26,27,28,30,31,32,33,34,35,37,38,39,40,42,43,44,45,46,48,49,50,51,53,54,55,56,58,59,0,2,3,4,5,7,8,9,11,12,13,15,16,17,19,20,21,23,24,25,27,28,29,31,32,33,35,36,37,39,40,41,43,44,48,46,48,49,50,51,52,53,55,56,57,58,59,0,1,2,3,4,5,5,6,7,8,8,9,10,10,11,11,12,12,13,13,13,14,14,14,14,14,14};
//MAY NEED TO: increment this SUNRISE_MINUTES array by one position to the left to account for the fact that sunrise is the day after the previous day's sunrise, will also need to adjust the sunrise_hour calculation in its function

void setup () {
  pinMode(light, OUTPUT);
  //switches use internal pullup resistors so the logic is inverted
  pinMode(lightswitch, INPUT_PULLUP);
  pinMode(functionswitch, INPUT_PULLUP);
  pinMode(LED_BUILTIN, OUTPUT);

  //initialize values for switch_state and last_switch_state to determine the current state of the switches
  //prevents lights from coming on when the program starts regardless of switch position
  if (digitalRead(lightswitch) == LOW) {
    switch_state = 0;
    last_switch_state = 0;
  }
  else {
    switch_state = 1;
    last_switch_state = 1;
  }

  #ifndef ESP8266
    while (!Serial); // for Leonardo/Micro/Zero
  #endif
  Serial.begin(9600);
  delay(3000); // wait for console opening

  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    while (1);
  }

  if (rtc.lostPower()) {
    Serial.println("RTC lost power, lets set the time!");
    // If the RTC have lost power it will sets the RTC to the date & time this sketch was compiled in the following line
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    //rtc.adjust(DateTime(2022, 1, 20, 17, 39, 0));
  }

  //lets user know when setup is finished. also makes sure light_off() sets a value for light_state
  light_on();
  delay(1000);
  light_off();
  delay(1000);
}

void loop () {  
  //"blink without delay" if statement, allows microcontroller to do other things (like toggle lamp states) in between figuring out what time it is
  unsigned long currentMillis = millis();
  if (digitalRead(functionswitch) == HIGH) {
    digitalWrite(LED_BUILTIN, HIGH);
    if (currentMillis - previousMillis >= interval) { //add switch here
      // save the time
      previousMillis = currentMillis;

      //set the time
      DateTime now = rtc.now();

      //convert rtc month/day to a day-of-year number
      day_number = calculate_day_number(now.month(), now.day());

      //set sunset_hour in a way that doesn't use so much memory, but increases processor demands
      //FIND A WAY TO DO THIS ONCE PER DAY and also when the program first starts
      sunset_hour = calculate_sunset_hour();
      sunrise_hour = calculate_sunrise_hour();

      //turn the lamp on/off if the program starts at a time when the lamp should be on
      //run this part only one time using the "run_once_flag" variable
      if (run_once_flag == 0) {
        //time between sunset and midnight
        if ((now.hour() == sunset_hour and now.minute() >= sunset_minutes[day_number]) or (now.hour() > sunset_hour and now.hour() < 23) or (now.hour() == 23 and now.minute() <= 59)) {
          light_on();
          switch_flag = 1;
        }
        //time between midnight and sunrise
        if ((now.hour() < sunrise_hour) or (now.hour() == sunrise_hour and now.minute() < sunrise_minutes[day_number])) {
          light_on();
          switch_flag = 1;
        }
        run_once_flag = 1;
      }
    
      //turn on the light at sunset time only if someone hasn't recently flipped the switch
        if (now.hour() == sunset_hour and now.minute() == sunset_minutes[day_number]) {
          light_on();
          switch_flag = 1;
        }
      //turn off the light at sunrise time
      if (now.hour() == sunrise_hour and now.minute() == sunrise_minutes[day_number]) {
        light_off();
        switch_flag = 0;
      }
    } 
  }
  else {
    digitalWrite(LED_BUILTIN, LOW);
    run_once_flag = 0; //reset the run_once_flag so that when the functionswitch is turned back on, the program checks to see what time it is again
  }

  //switch acts as a toggle for light state
  switch_state = digitalRead(lightswitch);
  //compare the current state with the previous state
  if (switch_state != last_switch_state) {
    if (light_state == LOW) { //if the light is off turn the light on
      light_on();
    }
    else { //if the light is on
      if (digitalRead(functionswitch) == HIGH) { //use a timer if in automatic mode
        if (switch_flag == 0) {  //but if the light was turned on manually during the day or the switch is in manual mode
          light_off(); //turn the light off
        }
        else { //if the light was turned on at night by the program set a "interval3"-length timer to turn the light back on if it is night time and the light was turned off but only if the switch is in automatic mode
          light_off(); //turn the light off
          timer_flag = 1;
          previousMillis3 = millis();
        }
      }
      else { //simply turn the light off if in manual mode
        light_off();
      }
    }
    delay(20);
    last_switch_state = switch_state;
  }

  //start the timer to turn the light back on if the function switch is in automatic mode and the timer flag has been set
  if (timer_flag == 1 && digitalRead(functionswitch) == HIGH) { //make sure not to turn the light back on if the function switch is flipped to manual after the timer is started
    currentMillis3 = millis();
    if (currentMillis3 - previousMillis3 >= interval3) {
      light_on();
      timer_flag = 0;
    }
  }
}

//might need to add a section here which acutally uses the interrupt_flag variable
void light_on(void) {
  light_state = HIGH;
  digitalWrite(light, HIGH);
}

void light_off(void) {
  light_state = LOW;
  digitalWrite(light, LOW);
}

//TIMES FOR CIVIL TWILIGHT
int calculate_sunset_hour() {
  if ((day_number <= 15) or (day_number > 304 and day_number <=367)) { //changed 16 to 15 here to account for day 0 in array
    sunset_hour = 16;
  }
  if ((day_number > 15 and day_number <= 62) or (day_number > 267 and day_number <= 304)) {
    sunset_hour = 17;
  }
  if ((day_number > 62 and day_number <= 109) or (day_number > 235 and day_number <= 267)) {
    sunset_hour = 18;
  }
  if ((day_number > 109 and day_number <= 162) or (day_number > 189 and day_number <= 235)) {
    sunset_hour = 19;
  }
  if (day_number > 162 and day_number <= 189) {
    sunset_hour = 20;
  }
  return sunset_hour;
}

int calculate_sunrise_hour() {
  if ((day_number <= 51) or (day_number > 316 and day_number <= 367)) {
    sunrise_hour = 6;
  }
  if ((day_number > 51 and day_number <= 86) or (day_number > 267 and day_number <= 316)) {
    sunrise_hour = 5;
  }
  if ((day_number > 86 and day_number <= 119) or (day_number > 218 and day_number <= 267)) {
    sunrise_hour = 4;
  }
  if (day_number > 119 and day_number <= 218) {
    sunrise_hour = 3;
  }
  return sunrise_hour;
}

//TIMES FOR SUNRISE/SUNSET
/*int calculate_sunrise_hour() {
  if ((day_number <= 29) or (day_number > 339 and day_number <= 366)) {
    sunrise_hour = 7;
  }
  if ((day_number > 29 and day_number <= 69) or (day_number > 292 and day_number <= 339)) {
    sunrise_hour = 6;
  }
  if ((day_number > 69 and day_number <= 102) or (day_number > 242 and day_number <= 292)) {
    sunrise_hour = 5;
  }
  if ((day_number > 102 and day_number <= 148) or (day_number > 183 and day_number <= 242)) {
    sunrise_hour = 4;
  }
  if (day_number > 148 and day_number <= 183) {
    sunrise_hour = 3;
  }
  return sunrise_hour;
}

int calculate_sunset_hour() {
  if ((day_number <= 39) or (day_number > 284 and day_number <= 340) or (day_number > 345 and day_number <= 366)) {
    sunset_hour = 16;
  }
  if ((day_number > 39 and day_number <= 85) or (day_number > 252 and day_number <= 284)) {
    sunset_hour = 17;
  }
  if ((day_number > 85 and day_number <= 135) or (day_number > 215 and day_number <= 252)) {
    sunset_hour = 18;
  }
  if (day_number > 135 and day_number <= 215) {
    sunset_hour = 19;
  }
  if (day_number > 340 and day_number <= 345) {
    sunset_hour = 15;
  }
  return sunset_hour;
}*/

int calculate_day_number(int month, int day) {
  int daysInMonth[] = {31,28,31,30,31,30,31,31,30,31,30,31};
  int day_of_year = 0;
  for (int i = 0; i < month -1; i++) {
    day_of_year += daysInMonth[i];
  }
  day_of_year += day;
  day_of_year = day_of_year - 1; //subtract one because day number starts at 0 but days start at 1 in real life duh;
  return day_of_year;
} 
